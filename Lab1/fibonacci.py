"""
    @file       fibonacci.py
    @brief      Computes the fibonacci number at index requested by user
    @details    Defines a function fib which computes the fibonacci number using
                a bottom-up looping technique and then calls the function to
                find the fibonacci number at the user's given input index.
    @author     Jonathan Cederquist
    @date       Last modified 1/21/21
"""

def fib(idx):
    """
        @brief   Calculates the fibonacci number at given index idx
        @param   idx An integer specifying the index of the desired fibonacci number
        @return  Returns the value of the fibonacci sequence at index idx
    """
    
    # Define seed values of f0 and f1
    if(idx==0):
        return 0
    
    elif(idx==1):
        return 1
    
    # If index is greater than 1, use bottom-up loop to keep track of
    # previous two values and add them to get desired fibonacci number
    else:
        one_back = 1
        two_back = 0
        fib = 0
    
        for x in range(2, idx+1):
            fib = two_back+one_back
            two_back = one_back
            one_back = fib
        
        return fib
    
    
if __name__ == '__main__':
    
    # Keep asking user for values indefinitely until Ctrl+C is pressed
    while(True):
        
        try:
            idx = input(('Welcome to the Fibonacci sequence calculator! '
                         'Press Ctrl+C at any time to exit. '
                         'Please enter an integer greater than or equal to '
                         'zero representing the index you would like printed: '))
        
        
            # Check that user input is made up of only digits
            # If not, pester user until they give a valid input
            while not(idx.isdigit()):
                print('Sorry bud, that is not a valid index. Please try again!')
                idx = input(('Please enter an integer greater than or equal to '
                              'zero representing the index you would like printed: '))
            
            
            # When input is valid, convert to an int and use fib function
            # to find and display fibonaccci number
            else:
                idx = int(idx)    
                print('The fibonacci number at index {:} is {:}.'.format(idx, fib(idx)))
                
                
        except KeyboardInterrupt:
            break
    
    print('See you later!')