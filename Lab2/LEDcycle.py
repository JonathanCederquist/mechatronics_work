'''
    @file       LEDcycle.py
    @brief      Allows user to cycle through 3 LED modes on the Nucleo
    @details    Implements a finite state machine, shown below, to cycle
                through 3 LED patterns of blinking, sine wave, and sawtooth
                on the Nucleo MCU when the user presses the main button.
    @image      html LEDDiag.png         
                See code here: https://bitbucket.org/JonathanCederquist/mechatronics_work/src/master/Lab2/LEDcycle.py
    @youtube    aHkoIBhmKXk&feature=youtu.be            
    @author     Jonathan Cederquist
    @date       Last modified 1/31/21
'''

import pyb
from math import sin, pi
import utime
import micropython

# Define functions here
def LED_SetDuty(percent):
    ''' @brief Sets LED duty cycle to a given percent
        @param percent A value 0 to 100 that represents the percent 'brightness'
                       of the LED
    '''
    
    #Set duty to percent calculated by equation
    timchan1.pulse_width_percent(percent)
            

def onButtonPress(interrupt_source):
    ''' @brief Resets button variable and starts timer when users presses button 
        @param interrupt_source The source of the external interrupt
    '''
    
    # Define button code here
    global button_press, button_tick
    button_press = True
    button_tick = utime.ticks_ms()



# Main program here
if __name__=="__main__":
    
    # Allocates space for error message 
    micropython.alloc_emergency_exception_buf(100)
    
    # Initialize variables and pins for controlling button and LED
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    timer2 = pyb.Timer(2, freq=20000)
    timchan1 = timer2.channel(1, pyb.Timer.PWM, pin=pinA5)
    button_press = False
    button_tick = 0
    
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPress)
    
    state = 0
    
    # Reset LED, print welcome and instructions
    LED_SetDuty(0)
    print('Hello and welcome to the LED pattern demonstration!')
    print('Please press the blue user button (B1) to transition between'
          ' patterns. Once the button is pressed, the pattern will play'
          ' and continue looping until the button is pressed again.'
          ' Then the LED will transition to the next pattern. Each time'
          ' the pattern changes, a message will appear with the name and'
          ' description of the pattern.'
          ' If you would like to exit at any time, please press Ctrl+C')
    print('Waiting for button press. . .')
    
    # Loop through states until user exits program with Ctrl + C
    while True:
        try:
            # Determine time since button press
            time_sec = (utime.ticks_diff(utime.ticks_ms(), button_tick))/1000
            
            if state==0:
                
                # If button is pressed, transition to next state and display pattern message
                if button_press:
                    button_press = False
                    state = 1
                    # Print pattern message for next state
                    print('This is the blinking mode. The LED blinks on for 0.5 secs,'
                      ' then off for 0.5 secs.')
                
            elif state==1:
                
                # Determine duty percent
                duty_percent = 100*(time_sec%1 < 0.5)
                
                # Update LED brightness   
                LED_SetDuty(duty_percent)
                
                # If button is pressed, transition to next state and display pattern message
                if button_press:
                    button_press = False
                    state = 2
                    # Print pattern message for next state
                    print('This is the sine wave mode. The LED starts at half brightness,'
                      ' then follows a sine wave pattern to full, half, off, and'
                      ' back to half brightness')
                
            elif state==2:
            
                # Determine duty percent
                duty_percent = 100*(0.5*sin((2*pi/10)*time_sec)+0.5)
                
                # Update LED brightness   
                LED_SetDuty(duty_percent)
                
                # If button is pressed, transition to next state and display pattern message
                if button_press:
                    button_press = False
                    state = 3
                    # Print pattern message for next state
                    print('This is the sawtooth mode. The LED starts off, increases'
                      ' to full brightness over 1 sec, then resets and starts over.')
                
            elif state==3:
                
                # Determine duty percent
                duty_percent = 100*(time_sec%1)
                
                # Update LED brightness   
                LED_SetDuty(duty_percent)
                
                # If button is pressed, transition to next state and display pattern message
                if button_press:
                    button_press = False
                    state = 1
                    # Print pattern message for next state
                    print('This is the blinking mode. The LED blinks on for 0.5 secs,'
                      ' then off for 0.5 secs.')
            
        except KeyboardInterrupt:
            break
    
    #De-initialization code here
    # Turn off interrupt
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=None)
    
    # Disable timer
    timchan1 = timer2.channel(1, pyb.Timer.OC_TIMING)
    
    # Set LED back to out
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
    
    print('Goodbye!')

