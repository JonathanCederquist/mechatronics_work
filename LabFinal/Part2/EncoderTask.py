''' @file       EncoderTask.py
    @brief      Controls the sampling of data from the encoder
    @details    Does some detailed stuff
    @author     Jonathan Cederquist
    @date       Last Modified 3/13/21
'''
import utime
import shares
import encoderDriver
import pyb

class EncoderTask:
    ''' @brief      A control task to query the encoder for data
        @details    Creates an encoderDriver object and updates the position at 
                    regular intervals to be stored in shares
    '''
    
    def __init__(self, period=50, DBG_flag=True):
        ''' @brief      Constructs an EncoderTask object
            @details    Creates two encoderDriver objects whose position will be
                        updated regularly using the update method 
            @param      period The period of the FSM in milliseconds
            @param      DBG_flag A boolean flag to turn on/off debug mode
        '''
        
        ## @brief Period of FSM in milliseconds if not omitted
        self.period = period
        
        ## @brief Debug flag to specify if not omitted
        self.DBG_flag = DBG_flag
    
        ## @brief Time of next iteration of FSM
        self.nextTime = utime.ticks_add(utime.ticks_ms(), self.period)
        
        ## @brief encoderDriver object 1
        self.enc1 = encoderDriver.encoderDriver(4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, DBG_flag=False)
        
        ## @brief encoderDriver object 2
        self.enc2 = encoderDriver.encoderDriver(8, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, DBG_flag=False)
        
    def run(self):
        ''' @brief  Updates the value of the encoder position in shares
        '''
        now = utime.ticks_ms()
        
        if utime.ticks_diff(now, self.nextTime) >= 0:
            self.nextTime = utime.ticks_add(now, self.period)
            
            # Continually update both encoders
            if self.DBG_flag:
                print('Updating Encoders')
                
            self.enc1.update()
            self.enc2.update()
            
            # Update position and delta values in shares
            shares.encoder1_Position = self.enc1.get_Position()
            shares.encoder2_Position = self.enc2.get_Position()
            
            shares.encoder1_Delta = self.enc1.get_Delta()
            shares.encoder2_Delta = self.enc2.get_Delta()
            
            if self.DBG_flag:
                print(shares.encoder1_Position)
        
        
        
        

