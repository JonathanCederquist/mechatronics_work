''' @file       main.py
    @brief      Creates and indefinitely runs tasks
    @author     Jonathan Cederquist
    @date       Last Modified 3/13/21
'''    

import UI_task
import EncoderTask
import shares

if __name__ == '__main__':
    
    UITask = UI_task.UI_task()
    EncoderTask = EncoderTask.EncoderTask(DBG_flag=False)
    
    
    while True:
        
        try:
            UITask.run()
            EncoderTask.run()
            
        except KeyboardInterrupt:
            break
    
    print('Goodbye!')
        
        