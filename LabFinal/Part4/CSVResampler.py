''' @file       CSVReSampler.py
    @brief      Resamples given CSV reference profiles at rate of ControlTask
    @author     Jonathan Cederquist
    @date       Last Modified 3/18/21
'''

import csv

if __name__ == '__main__':
    
    # Create csv reader/writer
    with open('referenceProfile.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        
        with open('reference.csv', newline='') as k:
            reader = csv.reader(k)
            
            count = 0
            
            for row in reader:
                if count%50==0:
                    writer.writerow(row)
                
                count += 1