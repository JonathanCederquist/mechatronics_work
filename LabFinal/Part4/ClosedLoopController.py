''' @file       ClosedLoopController.py
    @brief      Implements a proportional integral (PI) controller
    @details    See code here: https://bitbucket.org/JonathanCederquist/mechatronics_work/src/master/LabFinal/Part3/ClosedLoopController.py
    @author     Jonathan Cederquist
    @date       Last Modified 3/14/21
'''
class ClosedLoopController:
    ''' @brief Implements a simple proportional integral control
    '''
    
    def __init__(self, Kp, Ki, PWM_limit=100):
        ''' @brief Constructs a closed loop controller object with gain proportional gain Kp and integral game Ki
            @param Kp Initial gain of the proportional controller
            @param Ki Initial gain of the integral controller
            @param PWM_limit Max level of that pulse width modulation can be set
        '''
        
        ## @brief Proportional gain
        self.Kp = Kp
        
        ## @brief Integral gain
        self.Ki = Ki
        
        ## @brief Limit for max value of PWM
        self.PWM_limit = PWM_limit
        
    def run(self, desiredProValue, currentProValue, desiredIntValue, currentIntValue):
        ''' @brief  Computes the value of the output signal based on input values
            @param  desiredProValue The desired value to be obtained for proportional control
            @param  currentProValue The current value for proportional control
            @param  desiredIntValue The desired value to be obtained for integral control
            @param  currentIntValue The current value for integral control
            @return The output actuation value based on the gain and inputs
        '''
        errorPro = desiredProValue - currentProValue
        
        errorInt = desiredIntValue - currentIntValue
        
        toReturn = self.Kp*errorPro + self.Ki*errorInt
        
        if toReturn > -self.PWM_limit and toReturn < self.PWM_limit:
            return toReturn
        elif toReturn < -self.PWM_limit:
            return -self.PWM_limit
        else:
            return self.PWM_limit
    
    def get_Kp(self):
        ''' @brief  Returns the current value of proportional gain Kp
            @return The current value of Kp
        '''
        return self.Kp
    
    def set_Kp(self, newKp):
        ''' @brief Sets the proportional gain Kp to newKp
            @param newKp The new value for the gain Kp
        '''
        self.Kp = newKp
        
    def get_Ki(self):
        ''' @brief  Returns the current value of integral gain Ki
            @return The current value of Ki
        '''
        return self.Ki
    
    def set_Ki(self, newKi):
        ''' @brief Sets the integral gain Ki to newKi
            @param newKi The new value for the gain Ki
        '''
        self.Ki = newKi