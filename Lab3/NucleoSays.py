''' @file       NucleoSays.py
    @brief      Plays a game of Simon Says with the user
    @details    Implements a finite state machine, shown below, to display
                LED blink patterns on the Nucleo and check to see if user
                followed the pattern given by 'Simon'.
    @image      html SimonFSM.png
                See class code here: https://bitbucket.org/kkraybil/lab-3/src/master/NucleoSays.py
                <br>See main code here: https://bitbucket.org/kkraybil/lab-3/src/master/NucleoSaysTask.py
                <br>See demonstration here: https://youtu.be/sikEHtBlUGk
                The commit history is shown here:
    @image      html CommitHistory.png
    @author     Jonathan Cederquist
    @author     Kristin Kraybill-Voth
    @date       Last Modified 2/17/21
'''

# Import necessary modules
import random
import utime
from micropython import const
import pyb


class NucleoSays:
    '''
    @brief      Implements the finite state machine to play Simon Says
    @details    Holds dictionary of morse code letter and number values. 
                Randomly selects a pattern comprised of five letters. Blinks
                LED in accordance with the pattern, one letter at a time.
                Then accepts user input to repeat the pattern. Will interrupt 
                immediately if a user makes an error, or will continue to allow
                user to input the pattern until the sequence is completed. 
                After each  letteris completed, the pattern will 
                show an additional letter in the sequence. 
    '''
    
    #Define static variables here
    
    ##@brief Finite State 0 - initialization of the FSM
    S0_INIT = const(0)
    
    ##@brief Finite State 1 - displaying the LED pattern
    S1_DISPLAY_PATTERN = const(1)
    
    ##@brief Finite State 2 - waiting for user input via the button
    S2_USER_INPUT = const(2)
    
    ##@brief Finite State 3 - checks the user input against the pattern
    S3_CHECK_INPUT = const(3)
    
    ##@brief Finite State 4 - handles user losing the game
    S4_LOSE_GAME = const(4)
    
    ##@brief Finite State 5 - handles user winning the game
    S5_WIN_GAME = const(5)
    
    
    ##@brief List of letters used to generate random patterns
    letters = ['A','B','C','D','E',
           'F','G','H','I','J',
           'K','L','M','N','O',
           'P','Q','R','S','T',
           'U','V','W','X','Y',
           'Z','1','2','3','4',
           '5','6','7','8','9','0']
    
    ##@brief Dictionary representing the morse code chart 
    MORSE_CODE_DICT = { 'A':'._-', 'B':'-_._._.', 
                    'C':'-_._-_.', 'D':'-_._.', 'E':'.', 
                    'F':'._._-_.', 'G':'-_-_.', 'H':'._._._.', 
                    'I':'._.', 'J':'._-_-_-', 'K':'-_._-', 
                    'L':'._-_._.', 'M':'-_-', 'N':'-_.', 
                    'O':'-_-_-', 'P':'._-_-_.', 'Q':'-_-_._-', 
                    'R':'._-_.', 'S':'._._.', 'T':'-', 
                    'U':'._._-', 'V':'._._._-', 'W':'._-_-', 
                    'X':'-_._._-', 'Y':'-_._-_-', 'Z':'-_-_._.', 
                    '1':'.-_-_-_-', '2':'._._-_-_-', '3':'._._._-_-', 
                    '4':'._._._._-', '5':'._._._._.', '6':'-_._._._.', 
                    '7':'-_-_._._.', '8':'-_-_-_._.', '9':'-_-_-_-_.', 
                    '0':'-_-_-_-_-'}
    
    def __init__(self, period=100, timeUnit=500, DBG_flag=True, rig_Pat=False):
        ''' @brief      Constructs a NucleoSays object
            @details    Initializes variables for time control, counting of letters
                        and elements, and boolean flags for button pushes, dot, dash,
                        and space indicators
            @param      self The object that is being constructed
            @param      period The time period in milliseconds that controls how 
                        often the run() task happens
            @param      timeUnit The length of time in milliseconds that the unit
                        of a 'dot' is equal to
            @param      DBG_flag a boolean value control whether the object is
                        in debug mode or not
            @param      rig_Pat A boolean flag allowing the pattern to be rigged to only
                        two simple letters for demonstration purposes
        '''
                        
        ##@brief The current state of the finite state machine
        self.state = 0
        
        ##@brief Count number of runs of task
        self.runs = 0
        
        ##@brief Time unit length to specify if not omitted
        self.timeUnit = timeUnit
        
        ##@brief Flag to specify if not omitted
        self.DBG_flag = DBG_flag
        
        ##@brief Flag to specify if not omitted
        self.rig_Pat = rig_Pat
        
        ##@brief Period for task in milliseconds
        self.period = period
        
        ##@brief Timestamp for the next iteration of the task
        self.nextTime = utime.ticks_add(utime.ticks_ms(), self.period)
        
        ##@brief Counter for number of wins user has accumulated
        self.wins = 0
        
        ##@brief Counter for number of losses user has accumulated
        self.losses = 0
        
        ##@brief Counter for letter of current pattern
        self.letterCounter = 0
        
        ##@brief Counter for which element is being accessed
        self.elementCounter = 0
        
        ##@brief Counts when the letter has stopped
        self.letStopCounter = 0
        
        ##@brief Holds stop time for the pattern
        self.stopLet = 0
        
        ##@brief Boolean flag for last letter of pattern
        self.lastLetter = False
    
        ##@brief Dot flag if the user input was a dot
        self.Dot = False
        
        ##@brief Dash flag if the user input was a dash
        self.Dash = False
        
        ##@brief Space flag if the user input was a space
        self.Space = False
        
        ##@brief Invalid input flag if the user input was invalid
        self.invalid = False
        
        ##@brief Match flag if the user input matched the pattern element
        self.match = False
        
        ##@brief Initializes the Nucleo's LED Pin object
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
        
        ##@brief Initializes the Nucleo's button Pin object
        self.pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
        
        ##@brief Callback Interrupt for when the button is pressed OR released
        self.buttonInt = pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,
                                    pull=pyb.Pin.PULL_NONE, callback=self.onButtonPress)

        ##@brief Button push Boolean flag
        self.buttonPush = False
        
        ##@brief Button release Boolean flag
        self.buttonRelease = False
        
        ##@brief Time of button press
        self.buttonPushTime = utime.ticks_ms()
        
        ##@brief Time of button release
        self.buttonReleaseTime = utime.ticks_ms()
        
        ##@brief Generates string holding the pattern
        self.patternString = self.wordToMorse(self.genPattern(5))
        
        
        ##@brief Chops the pattern into array of each letter
        letterDict = self.letterChop(self.patternString,5)
        
        # Rigged pattern to help with testing
        if rig_Pat:
            self.patternString = '- . '
            letterDict = self.letterChop(self.patternString,2)
            
        ##@brief List to hold pattern as array of letters
        self.pattern = letterDict['letters']
        
        ##@brief Array of how many elements in each letter of the patterns
        self.numElements = letterDict['numElements']
        
        # Debug flag to help identify the pattern and number of elements
        if DBG_flag:
            print(self.pattern)
            print(self.patternString)
            print(self.numElements)
        
        ##@brief List of stop times for display state
        self.stopTimes = self.morseToLED(self.pattern)['StopTime']
        
        ##@brief List of LED levels for display state
        self.LEDLevels = self.morseToLED(self.pattern)['Levels']
        
        
    def run(self):
        
        ##@brief Tracks the overall runtime of the program
        now = utime.ticks_ms()
        
        # Runs program at the specified period
        if utime.ticks_diff(now, self.nextTime) >= 0:
            self.nextTime = utime.ticks_add(now, self.period)
        
            # Keeps track of number of runs of the FSM 
            self.runs += 1
            
            # Debugging flags to show current state and which run of FSM
            if(self.DBG_flag):
                print('S'+str(self.state) + ': R' + str(self.runs))
            
            #Finite State Machine Implemented Here:
            
            if self.state==self.S0_INIT:
                #run state 0 (init) code
                if self.runs == 1:
                    print('Welcome to Nucleo Says!')
                    print('Here are the instructions of how to play the game:')
                    print('Watch for the LED on the Nucleo to flash in a pattern of dots and dashes.')
                    print('After the lED finishes flashing, you must press the blue button in the same pattern')
                    print('Be careful-- timing matters!')
                    print('After you match the sequence, it will get longer.')
                    print('Match the full pattern to win.')
                    print('Please press the button to begin. . .')
                self.LED_Off()
                
                # Detects push of button to move to next state
                if self.buttonRelease:
                    # reset button flag
                    self.buttonRelease = False
                    # reset counter for letter stop time
                    self.letStopCounter = 0
                    self.stopLet = self.stopTimes[self.letterCounter]
                    
                    self.transitionTo(self.S1_DISPLAY_PATTERN)
            
            
            elif self.state==self.S1_DISPLAY_PATTERN:
                # run state 1 (display pattern) code
                
                # flags if on the last letter of the pattern
                if self.letterCounter == 4 or (self.rig_Pat and self.letterCounter == 1):
                    self.lastLetter = True
                
                # Uses list of LED levels to show the morse code pattern
                if self.letStopCounter < self.stopLet:
                    if self.LEDLevels[self.letStopCounter]==1:
                        self.LED_On()
                    elif self.LEDLevels[self.letStopCounter]==0:
                        self.LED_Off()
                        
                    self.letStopCounter += 1
                    
                else:
                    print('Pattern has finished displaying')
                    print('Please copy the sequence with the user button')
                    
                    # Transitions to user input state
                    self.transitionTo(self.S2_USER_INPUT)
                
                
            elif self.state==self.S2_USER_INPUT:
                # run state 2 (user input) code
                
                # On the first element of the sequence, waits for the button
                # to be pushed and released before the user input is checked,
                # but for all other elements, the input is checked on either 
                # press or release
                if (self.elementCounter ==  0 and self.buttonRelease) or (self.elementCounter !=0 and (self.buttonRelease or self.buttonPush)):
                        # reset button flags
                        self.buttonPush = False
                        self.buttonRelease = False
                        
                        # Run the translate time method, passing in push and 
                        # release times in order to identify if the user input 
                        # was a dot, dash or space
                        self.translateTime(self.buttonPushTime, self.buttonReleaseTime)
                        
                        # then transition to state 3
                        self.transitionTo(self.S3_CHECK_INPUT)
                
                
            elif self.state==self.S3_CHECK_INPUT:
                # run state 3 (check input) method
                self.checkInput() 
                
                # checks if user matched the element and the sequence is not over
                if self.match and self.fullStage == False:
                    self.match = False
                    # user must continue to enter the pattern
                    print('You matched the element, please enter next element')
                    self.transitionTo(self.S2_USER_INPUT)
                
                # checks if user matched element and sequence is over
                elif not(self.lastLetter) and self.match and self.fullStage:
                    # Requires user to press button to show the next sequence
                    print('You matched the full letter. Please press the button to view the next sequence.')
                    if self.buttonRelease:
                        # Makes sure any flags/counters are reset
                        self.buttonRelease = False
                        self.elementCounter = 0
                        self.fullStage = False
                        self.match = False
                        # sets up to display the next phase of the sequence
                        # Increment Letter counter
                        self.letterCounter += 1
                        self.letStopCounter = 0
                        self.stopLet = self.stopTimes[self.letterCounter]
                        
                        self.transitionTo(self.S1_DISPLAY_PATTERN)                   
                        
                    
                # if the check input returns that the user input matches the 
                # element in the sequence and it is the last element of the sequence, 
                # then the user has won the round (go to state 5)
                elif self.match and self.lastLetter and self.fullStage:
                    self.match = False
                    self.wins += 1
                    print('Congratulations! You won this round.')
                    print('W: ' + str(self.wins) + ' L: ' + str(self.losses))
                    print('If you would like to exit, please press Ctrl+c.')
                    print('If you would like to play again, please press the blue button,')
                    self.transitionTo(self.S5_WIN_GAME)
                    
                # if the user gave an invalid input, the user immediately loses
                elif self.invalid:
                    self.invalid = False
                    self.losses += 1
                    print("I'm sorry, you lost the game because of an invalid input.")
                    print('Your record is:')
                    print('W: ' + str(self.wins) + ' L: ' + str(self.losses))
                    print('If you would like to exit, please press Ctrl+c.')
                    print('If you would like to play again, please press the blue button,')
                    self.transitionTo(self.S4_LOSE_GAME)
                
                # if the user input did not match, the user immediately loses the game
                elif not(self.match):
                    self.match = False
                    self.losses += 1
                    print("I'm sorry, you lost the game because you did not match the pattern.")
                    print('Your record is:')
                    print('W: ' + str(self.wins) + ' L: ' + str(self.losses))
                    print('If you would like to exit, please press Ctrl+c.')
                    print('If you would like to play again, please press the blue button,')
                    self.transitionTo(self.S4_LOSE_GAME)
    
            
            elif self.state==self.S4_LOSE_GAME:
                # run state 4 (lose game) code
                
                # Resets the system to play again if the button is pushed
                if self.buttonRelease:
                    
                    # Reset flags
                    self.lastLetter = False
                    self.Dot = False
                    self.Dash = False
                    self.Space = False
                    self.invalid = False
                    self.match = False
                    self.buttonPush = False
                    self.buttonRelease = False
                    
                    # Reset letter counter and element counter 
                    self.letterCounter = 0
                    self.elementCounter = 0
                    
                    # Regenerate patern 
                    letterDict = self.letterChop(self.wordToMorse(self.genPattern(5)),5)
        
                    # List to hold pattern
                    self.pattern = letterDict['letters']
                    
                    # Array of how many elements in each letter of the patterns
                    self.numElements = letterDict['numElements']
                    
                    # List of stop times for display state
                    self.stopTimes = self.morseToLED(self.pattern)['StopTime']
                    
                    # Reset letStopCounter, stopLet
                    self.letStopCounter = 0
                    self.stopLet = self.stopTimes[self.letterCounter]
                    
                    # List of LED levels for display state
                    self.LEDLevels = self.morseToLED(self.pattern)['Levels']
                    
                    # Print pattern
                    if self.DBG_flag:
                        print(self.pattern)
                    
                    # Transition to state 1
                    self.transitionTo(self.S1_DISPLAY_PATTERN)
            
            elif self.state==self.S5_WIN_GAME:
                # run state 5 (win game) code
                
                # Resets the system to play again if the button is pushed
                if self.buttonRelease:
                    
                    # Reset flags
                    self.lastLetter = False
                    self.Dot = False
                    self.Dash = False
                    self.Space = False
                    self.invalid = False
                    self.match = False
                    self.buttonPush = False
                    self.buttonRelease = False
                    
                    # Reset letter counter and element counter 
                    self.letterCounter = 0
                    self.elementCounter = 0
                    
                    # Regenerate patern 
                    letterDict = self.letterChop(self.wordToMorse(self.genPattern(5)),5)
        
                    # List to hold pattern
                    self.pattern = letterDict['letters']
                    
                    # Array of how many elements in each letter of the patterns
                    self.numElements = letterDict['numElements']
                    
                    # List of stop times for display state
                    self.stopTimes = self.morseToLED(self.pattern)['StopTime']
                    
                    # Reset letStopCounter, stopLet
                    self.letStopCounter = 0
                    self.stopLet = self.stopTimes[self.letterCounter]
                    
                    # List of LED levels for display state
                    self.LEDLevels = self.morseToLED(self.pattern)['Levels']
                    
                    # Print pattern
                    if self.DBG_flag:
                        print(self.pattern)
                    
                    # Transition to state 1
                    self.transitionTo(self.S1_DISPLAY_PATTERN)
                
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
    
    def transitionTo(self, newState):
        ''' @brief Changes the state of the FSM to newState
            @param self The object calling the method
            @param newState A constant representing the state to transition to
        '''
        
        if self.DBG_flag:
            print(str(self.state) + '->' + str(newState))
        self.state = newState

    
    def onButtonPress(self, interrupt_source):
        ''' @brief  Flips flags, starts timers, flashes LED when button is pressed or released
            @param  self The object calling the method
            @param  interrupt_source The source of the external interrupt
        '''
        
        if self.pinC13.value() == 0:
            # when the button is pushed, the button pushed flag is true 
            # and the button released flag is false
            self.buttonPush = True
            self.pinA5.high()
            self.buttonRelease = False
            
            # starts timer from when the button was pushed
            self.buttonPushTime = utime.ticks_ms()
            
        elif self.pinC13.value() == 1:
            # when the button is released, the button released flag is true
            # and the button pushed flag is false
            self.pinA5.low()
            self.buttonRelease = True
            self.buttonPush = False
            
            # starts timer from when button was released
            self.buttonReleaseTime = utime.ticks_ms()

            
    def LED_On(self):
        ''' @brief  Turns the LED on to full brightness
            @param  self The object calling the method
        '''
        self.pinA5.high()
        
        
    def LED_Off(self):
        ''' @brief  Turns the LED off to zero brightness
            @param  self The object calling the method
        '''
        self.pinA5.low()
        
        
    def translateTime(self, pushTime, releaseTime):
        ''' @brief  Translates two button interrupt times into a dot, dash, or space
            @param  self The object calling the method
            @param  pushTime A utime stamp in milliseconds when the button was pushed
            @param  releaseTime A utime stamp in milliseconds when the button was released
        '''
        
        # checks release time - push time is within 25% of one time unit
        # in which case input was a dot
        if utime.ticks_diff(releaseTime, pushTime) > 0.75*self.timeUnit and utime.ticks_diff(releaseTime, pushTime) < 1.25*self.timeUnit:
            self.Dot = True
            print('Dot input received')
            
        # checks release time - push time is within 25% of 3 time units
        # in which case the input was a dash
        elif utime.ticks_diff(releaseTime, pushTime) > 0.75*3*self.timeUnit and utime.ticks_diff(releaseTime, pushTime) < 1.25*3*self.timeUnit:
            self.Dash = True
            print('Dash input received')
            
        # checks push time - release time is within 25% of one time unit
        # in which case the user left a space
        elif utime.ticks_diff(pushTime, releaseTime) > 0.6*self.timeUnit and utime.ticks_diff(pushTime, releaseTime) < 1.4*self.timeUnit:
            self.Space = True
            print('Space input received')
        
        # if the user has given any inputs outside of this, it's an incorrect
        # input
        else:
            self.invalid = True
            print('Invalid input')
        
    def genPattern(self,length):
        ''' @brief      Generates a random string with given length
            @details    Reads an unused pin to pick a random seed and choose
                        characters out of the available bank
            @param      self The class object calling the method
            @param      length An int for the number of characters in the random
                        string
            @return     A random string of length 'length'
        '''
        
        #Define unused pin for generating random nums
        pinRand = pyb.ADC(pyb.Pin.cpu.A0)
        patternStr = ''
        
        for x in range(length):
            random.seed(pinRand.read())
            patternStr += random.choice(self.letters)
            
        return patternStr
    
    def wordToMorse(self, randWord):
        ''' @brief  Converts a given alpha-numeric string to morse code
            @param  self The object calling the method
            @param  randWord A random string to be converted to morse code
            @return A morse code representation of the given string
        '''
        
        # Method and morse code dictionary adapted from 
        # https://www.geeksforgeeks.org/morse-code-translator-python/
        morse = '' 
        
        for letter in randWord: 
            morse += self.MORSE_CODE_DICT[letter] + ' '
    
        return morse
    
    def letterChop(self, code, numLetts):
        ''' @brief  Splits a string of morse code into separate array entries for each letter
            @param  self The object calling the method
            @param  code A string of morse code to be 'sliced up'
            @param  numLetts A int representing the number of letters the morse code string
            @return A dictionary containing an array of morse code letters and an array
                    of numbers corresponding to the total number of elements up to that
                    point in the pattern
        '''

        letter = ['']*numLetts
        elementsUpToNow = [0]*numLetts
        space = 0
        nextSpace = code.find(' ', space)
        
        for i in range(numLetts):
            letter[i] = code[space:nextSpace]

            space = nextSpace+1
            nextSpace = code.find(' ', space)
        
        elementsUpToNow[0] = len(letter[0])
        
        for j in range(1, numLetts):
            elementsUpToNow[j] = elementsUpToNow[j-1]+len(letter[j])+1
        
            
        letInfo = {'letters':letter, 'numElements':elementsUpToNow}
        return letInfo
    
    def morseToLED(self, patternList):
        ''' @brief      Converts array of morse code letters to LED levels based on given time parameters
            @details    Provides an array of one and zeros corresponding to LED levels of on
                        or off based on the given time unit length and period when the run()
                        task is executed
            @param      self The object calling the method
            @param      patternList An array on separate morse code letters to translate to LED levels
            @return     A dictionary with two arrays. The first, called 'StopTime', is an array
                        of equal length to patternList containing the indices where each letter
                        ends in the time array. The second, called 'Levels', is an array of ones
                        and zeros that represents when the LED should be on or off
        '''
        period = self.period
        timeUnit = self.timeUnit
        levelArray = []
        stopInds = [0]*len(patternList)
        charsPerUnit = int(timeUnit/period)
        stopCounter = 0
        
        for a in range(len(patternList)):
        
            for b in range(len(patternList[a])):
                
                if patternList[a][b:b+1]=='.':
                    stopCounter += charsPerUnit
                    
                    for c in range(charsPerUnit):
                        levelArray.append(1)
                        
                elif patternList[a][b:b+1]=='-':
                    stopCounter += 3*charsPerUnit
                    
                    for d in range(charsPerUnit*3):
                        levelArray.append(1)
                
                elif patternList[a][b:b+1]=='_':
                    stopCounter += charsPerUnit
                    
                    for count in range(charsPerUnit):
                        levelArray.append(0)
            
            stopCounter += charsPerUnit
            
            for e in range(charsPerUnit):
                levelArray.append(0)
                    
            stopInds[a] = stopCounter
                    
            #Could add more spaces between letters
            #With a set for loop adding 3?*char of 0
                    
        timez ={'StopTime':stopInds, 'Levels':levelArray}
        return timez
    
    def checkInput(self):
        ''' @brief  Checks the user input to determine if it matches the displayed pattern
            @param  self The object calling the method
        '''
        
        # Checks type of input provided by user against the corresponding 
        # element using the Boolean flags tripped in the translation method
        
        if self.Dot:
            # if the user input a dot, it has to be matched against the
            # element in the pattern
            # Turn off flag
            self.Dot = False
            print (self.patternString[self.elementCounter])
            if self.patternString[self.elementCounter] == '.':
                print('Your input matches the DOT in the pattern')
                self.match = True
            else:
                print('Incorrect input')
                self.match = False
        
        elif self.Dash:
            self.Dash = False
            print (self.patternString[self.elementCounter])
            if self.patternString[self.elementCounter] == '-':
                print('Your input matches the DASH in the pattern')
                self.match = True
            else:
                print('Incorrect input')
                self.match = False
        
        elif self.Space:
            self.Space = False
            print (self.patternString[self.elementCounter])
            if self.patternString[self.elementCounter] == '_' or self.patternString[self.elementCounter] == ' ':
                print('Your pause matches the space in the pattern')
                self.match = True
            else:
                print('Incorrect input')
                self.match = False
                
        # Check which element of the pattern we're on - if at end of letter:
        if self.numElements[self.letterCounter] == (self.elementCounter+1):
            self.fullStage = True
        
        # If not at end of letter
        else:
            self.fullStage = False
            self.elementCounter += 1
        
        if self.DBG_flag:
            print('Element: ' + str(self.elementCounter))
            
    def deInit(self):
        ''' @brief Resets button callback and LED pin
            @param self The object calling the method
        '''
        self.buttonInt = pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,
                                    pull=pyb.Pin.PULL_NONE, callback=None)
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        
        