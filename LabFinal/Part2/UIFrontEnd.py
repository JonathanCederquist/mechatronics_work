''' @file       UIFrontEnd.py
    @brief      Queries user for input to control data collection
    @details    Communicates with UI_task running on Nucleo to accept user
                input to control data collection of a simple decaying sine
                function
                <br>See code here: https://bitbucket.org/JonathanCederquist/mechatronics_work/src/master/LabFinal/UIFrontEnd.py
    @author     Jonathan Cederquist
    @date       Last modified 3/15/2021
'''

import serial
from matplotlib import pyplot
from array import array
import keyboard
import csv

def onKeyPress(key):
        ''' @brief Callback function to run when key is pressed.
            @param key The key that is pressed
        '''
        global key_pressed 
        key_pressed = key.name 
        

if __name__ == '__main__':
    
    #Set up Nucleo link
    ser = serial.Serial(port='COM7', baudrate=115273, timeout=2)
    timeValues = []
    funcValues = []
    key_pressed = None
    
    ## Turn on callback for particular keys only
    keyboard.on_release_key('s', callback=onKeyPress)
    keyboard.on_release_key('g', callback=onKeyPress)
    keyboard.on_release_key('z', callback=onKeyPress)
    keyboard.on_release_key('p', callback=onKeyPress)
    keyboard.on_release_key('d', callback=onKeyPress)
    
    #Create CSV Writer
    
        
        
    try:
        print('Welcome to the simple data collection demonstration!')
        print('Please input a ''G'' to begin gathering data. The Nucleo will then start gathering'
              ' time values to populate a function')
        print('To stop the data collection, simply press ''S'' or wait for 30 seconds'
              ' Either of those events will initiate data transfer.')
        print('Once the data has been sent,  the script will generate a plot of the function'
              ' and a CSV file of the data.')
        
        
        while True:
            
            if key_pressed == 'g':
                ser.write(key_pressed.encode())
                key_pressed = None
                print('Thank you for starting data collection. Please wait while data is collected. . .')
                break
            elif key_pressed == 'z':
                print('You pressed z!')
                ser.write(key_pressed.encode())
                key_pressed = None
                print('Encoder position has been zeroed')
            elif key_pressed == 'p':
                print('You pressed p!')
                ser.write(key_pressed.encode())
                key_pressed = None
                try:
                    value = ser.readline().decode()
                    print('Encoder position is: {:} rad'.format(value))
                except:
                    print('something went wrong')
            elif key_pressed == 'd':
                print('You pressed d!')
                ser.write(key_pressed.encode())
                key_pressed = None
                try:
                    value = ser.readline().decode()
                    print('Encoder delta is: {:} rad'.format(value))
                except:
                    print('something went wrong')
            elif key_pressed != 'g':
                key_pressed = None

        
        t_Max = 0
        
        while ser.in_waiting == 0:
            if key_pressed == 's':
                    ser.write(key_pressed.encode())
                    key_pressed = None
                    break
            elif key_pressed == 'z':
                print('You pressed z!')
                ser.write(key_pressed.encode())
                key_pressed = None
                print('Encoder position has been zeroed')
            elif key_pressed == 'p':
                print('You pressed p!')
                ser.write(key_pressed.encode())
                key_pressed = None
                try:
                    value = ser.readline().decode()
                    print('Encoder position is: {:} rad'.format(value))
                except:
                    print('something went wrong')
            elif key_pressed == 'd':
                print('You pressed d!')
                ser.write(key_pressed.encode())
                key_pressed = None
                try:
                    value = ser.readline().decode()
                    print('Encoder delta is: {:} rad'.format(value))
                except:
                    print('something went wrong')
            t_Max += 1
            if t_Max > 10000000:
                print('Sorry, the serial line took too long to respond')
                break
        
        count = 0
        
        
        myStr = ser.readline().decode()
        while myStr != '':
            print('Receiving Data')
            myStr.strip()
            mySplit = myStr.split(', ')

            
            timeValues.append(float(mySplit[0]))
            funcValues.append(float(mySplit[1]))
            
            myStr = ser.readline().decode()
            
            
            count += 1
        
        print(len(timeValues))
        timeValuesArray = array('f', [0]*len(timeValues))
        funcValuesArray = array('f', [0]*len(funcValues))
        
        with open('data.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            for i in range(len(timeValuesArray)):
                timeValuesArray[i] = timeValues[i]
                funcValuesArray[i] = funcValues[i]
                writer.writerow([str(timeValuesArray[i]), str(funcValuesArray[i])])
       
        
        print('Finished collecting data, now generating a plot') 
        pyplot.figure(1)
        pyplot.plot(timeValuesArray, funcValuesArray)
        axes=pyplot.gca()
        axes.relim()
        axes.autoscale_view()
        pyplot.draw()
        pyplot.xlabel('Time [s]')
        pyplot.ylabel('Encoder Position [rad]')
        pyplot.savefig('DataPlot.png')
            
    except KeyboardInterrupt:
        pass
    
    #De init code here
    ser = serial.Serial(port=None)
    keyboard.unhook_all()