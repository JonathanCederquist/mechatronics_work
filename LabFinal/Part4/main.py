''' @file       main.py
    @brief      Creates and indefinitely runs tasks
    @details    See code here: https://bitbucket.org/JonathanCederquist/mechatronics_work/src/master/LabFinal/Part3/main.py
    @author     Jonathan Cederquist
    @date       Last Modified 3/18/21
'''    

import UI_task
import ControlTask
import shares

if __name__ == '__main__':
    
    UITask = UI_task.UI_task()
    ControlTask = ControlTask.ControlTask()
    
    
    while True:
        
        try:
            UITask.run()
            ControlTask.run()
            
        except KeyboardInterrupt:
            break
    
    print('Goodbye!')
        
        