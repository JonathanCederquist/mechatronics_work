'''
    @file       elevator.py
    @brief      Simulates a simple model of an elevator
    @details    Implements a finite state machine, shown below, to simulate
                behaivor of a simple elevator that can only move between two
                floors
    @image      html ElevatorDiag.png
                See code here:
    @author     Jonathan Cederquist
    @date       Last modified 1/21/21
'''

import random
import time

# Function definitions go here
def motor(cmd):
    '''
        @brief  Controls the motion of the motor
        @param  cmd An int describing the desired state for the motor,
                0=stop, 1=up, 2=down
    '''
    if cmd==0:
        print('Motor stop')
    elif cmd==1:
        print('Motor up')
    elif cmd==2:
        print('Mot down')


def first():
    '''
        @brief      Simulates the first floor sensor
        @return     A random boolean value corresponding to whether the 
                    elevator is detected at the first floor
    '''
    return random.choice([True, False]) #Randomly returns T or F


def second():
    '''
        @brief      Simulates the second floor sensor
        @return     A random boolean value corresponding to whether the 
                    elevator is detected at the second floor
    '''
    return random.choice([True, False]) #Randomly returns T or F


def button_1():
    '''
        @brief      Simultates button for first floor
        @return     A random value corresponding to whether the
                    button for floor one is pressed
    '''
    return random.choice([0, 1]) #Randomly returns 0 or 1

def button_2():
    '''
        @brief      Simultates button for second floor
        @return     A random value corresponding to whether the
                    button for floor two is pressed
    '''
    return random.choice([0, 1]) #Randomly returns 0 or 1
    
# Main program/test program begin
# Only runs if script is executed as main by pressing play
# but does not run if the script is imported as a module
if __name__ == "__main__":
    #Program initialiation goes here
    state = 0 #Initial state is init state
    button1 = random.choice([0,1])
    button2 = random.choice([0,1])
    
    while True:
        try:
            #main program code goes here
            if state==0:
                #run state 0 (init) code
                print('S0')
                
                motor(2) # Command motor to go down
                button1 = 0 # Reset buttons
                button2 = 0 
                state = 1 # Update state for next iteration
                
            elif state==1:
                # run state 1 (moving down) code
                print('S1')
                
                # If we are at floor 1, stop the motor, reset button, and transition to S2
                if first():
                    motor(0)
                    button1 = 0
                    state = 2
                
            elif state==2:
                # run state 2 (stopped at floor 1) code
                print('S2')
                
                #Simulate user input
                button1 = button_1()
                button2 = button_2()
                
                # If button1 is pressed, reset button and do nothing
                if button1 == 1:
                    button1 = 0
                    
                # If button2 is pressed, start motor going up and transition to S3
                if button2 == 1:
                    motor(1)
                    state = 3
                
            elif state==3:
                # run state 3 (moving up) code
                print('S3')
                
                # If we are at floor 2, stop the motor, reset button, and transition to S4
                if second():
                    motor(0)
                    button2 = 0
                    state = 4
            
            elif state==4:
                # run state 4 (stopped at floor 2) code
                print('S4')
                
                # Simulate user input
                button1 = button_1()
                button2 = button_2()
                
                # If button2 is pressed, reset button and do nothing
                if button2 == 1:
                    button2 = 0
                    
                # If button1 is pressed, start motor going down and transition to S1
                if button1 == 1:
                    motor(2)
                    state = 1                
                
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
                
            # Slow down FSM execution so we can see output in console
            time.sleep(0.2)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            break
        
    #Program de-initialization goes here