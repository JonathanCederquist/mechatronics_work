''' @file       shares.py
    @brief      Container file holding all relevant variables for motor control project
    @author     Jonathan Cederquist
    @date       Last Modified 3/11/21
'''
## Relevant Variables

# Encoder positions in radians
encoder1_Position = 0
encoder2_Position = 0

# Encoder Delta in radians
encoder1_Delta = 0
encoder2_Delta = 0

# Encoder Speed
encoder1_Velocity = 0
encoder2_Velocity = 0

#Desired speed of encoder
omega_des = 0

# Performance Metric
J = 0