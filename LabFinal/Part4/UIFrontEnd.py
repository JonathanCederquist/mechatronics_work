''' @file       UIFrontEnd.py
    @brief      Queries user for input to control data collection
    @details    Communicates with UI_task running on Nucleo to accept user
                input to control data collection of a encoder speed
                <br>See code here: https://bitbucket.org/JonathanCederquist/mechatronics_work/src/master/LabFinal/Part3/UIFrontEnd.py
                <br>See example CSV here: https://bitbucket.org/JonathanCederquist/mechatronics_work/src/master/LabFinal/Part3/data.csv
                <br>An example progression of plots generated and sample calculation of Kp:
    @image      html TimeConstantCalcs.PNG
                <br>With Kp = 0.4
    @image      html DataPlotP1.png
                <br>With Kp = 0.8
    @image      html DataPlotP2.png
                <br>With Kp = 1.1
    @image      html DataPlotP3.png
    @author     Jonathan Cederquist
    @date       Last modified 3/18/2021
'''

import serial
from matplotlib import pyplot
from array import array
import keyboard
import csv

def onKeyPress(key):
        ''' @brief Callback function to run when key is pressed.
            @param key The key that is pressed
        '''
        global key_pressed 
        key_pressed = key.name 
        

if __name__ == '__main__':
    
    #Set up Nucleo link
    ser = serial.Serial(port='COM7', baudrate=115273, timeout=2)
    timeValues = []
    funcValues = []
    PosValues = []
    refVel = []
    refPos = []
    
    key_pressed = None
    
    ## Turn on callback for particular keys only
    keyboard.on_release_key('s', callback=onKeyPress)
    keyboard.on_release_key('g', callback=onKeyPress)
    keyboard.on_release_key('z', callback=onKeyPress)
    keyboard.on_release_key('p', callback=onKeyPress)
    keyboard.on_release_key('d', callback=onKeyPress)
    
    #Create CSV Writer
    
        
        
    try:
        print('Welcome to the simple data collection demonstration!')
        print('Please input a ''g'' to begin gathering data. The Nucleo will then start gathering'
              ' values of encoder positions')
        print('You may also press ''p'', ''d'', or ''z'' at any time to print the encoder position,'
              ' encoder delta, or zero the encoder, respectively.')
        print('To stop the data collection, simply press ''s'' or wait for 30 seconds'
              ' Either of those events will initiate data transfer.')
        print('Once the data has been sent,  the script will generate a plot of the encoder position'
              ' and a CSV file of the data.')
        
        # Is it ok to have blocking code for gain?
        Kp = 'k'+input('Please enter a value for the proportional gain as a decimal number:')
        ser.write('{:}\r\n'.format(Kp).encode())
        
        Ki = 'i'+input('Thank you. Now, please enter a value for the integral gain as a decimal number:')
        ser.write('{:}\r\n'.format(Ki).encode())
        
        key_pressed = 'g' #Manually rig while loop?
        
        
        while True:
            
            if key_pressed == 'g':
                ser.write(key_pressed.encode())
                key_pressed = None
                print('Thank you for starting data collection. Please wait while data is collected. . .')
                break
            elif key_pressed == 'z':
                print('You pressed z!')
                ser.write(key_pressed.encode())
                key_pressed = None
                print('Encoder position has been zeroed')
            elif key_pressed == 'p':
                print('You pressed p!')
                ser.write(key_pressed.encode())
                key_pressed = None
                try:
                    value = ser.readline().decode()
                    print('Encoder position is: {:} rad'.format(value))
                except:
                    print('something went wrong')
            elif key_pressed == 'd':
                print('You pressed d!')
                ser.write(key_pressed.encode())
                key_pressed = None
                try:
                    value = ser.readline().decode()
                    print('Encoder delta is: {:} rad'.format(value))
                except:
                    print('something went wrong')
            elif key_pressed != 'g':
                key_pressed = None

        
        t_Max = 0
        
        while ser.in_waiting == 0:
            if key_pressed == 's':
                    ser.write(key_pressed.encode())
                    key_pressed = None
                    break
            elif key_pressed == 'z':
                print('You pressed z!')
                ser.write(key_pressed.encode())
                key_pressed = None
                print('Encoder position has been zeroed')
            elif key_pressed == 'p':
                print('You pressed p!')
                ser.write(key_pressed.encode())
                key_pressed = None
                try:
                    value = ser.readline().decode()
                    print('Encoder position is: {:} rad'.format(value))
                except:
                    print('something went wrong')
            elif key_pressed == 'd':
                print('You pressed d!')
                ser.write(key_pressed.encode())
                key_pressed = None
                try:
                    value = ser.readline().decode()
                    print('Encoder delta is: {:} rad'.format(value))
                except:
                    print('something went wrong')
            t_Max += 1
            if t_Max > 10000000:
                print('Sorry, the serial line took too long to respond')
                break
        
        count = 0
        
        
        myStr = ser.readline().decode()
        while myStr != '':
            print('Receiving Data')
            myStr.strip()
            mySplit = myStr.split(', ')

            
            timeValues.append(float(mySplit[0]))
            funcValues.append(float(mySplit[1]))
            refVel.append(float(mySplit[2]))
            refPos.append(float(mySplit[3]))
            PosValues.append(float(mySplit[4]))
            
            myStr = ser.readline().decode()
            
            
            count += 1
        
        print(len(timeValues))
        timeValuesArray = array('f', [0]*len(timeValues))
        funcValuesArray = array('f', [0]*len(funcValues))
        refVelArray = array('f', [0]*len(refVel))
        refPosArray = array('f', [0]*len(refPos))
        PosValuesArray = array('f', [0]*len(PosValues))
        
        with open('data.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            for i in range(len(timeValuesArray)):
                timeValuesArray[i] = timeValues[i]
                funcValuesArray[i] = funcValues[i]
                refVelArray[i] = refVel[i]
                refPosArray[i] = refPos[i]
                PosValuesArray[i] = PosValues[i]
                writer.writerow([str(timeValuesArray[i]), str(funcValuesArray[i]), str(refVelArray[i]), str(refPosArray[i]), str(PosValuesArray[i])])
       
        
        print('Finished collecting data, now generating a plot') 
        pyplot.figure(1)
        pyplot.subplot(2, 1, 1)
        pyplot.plot(timeValuesArray, funcValuesArray)
        pyplot.plot(timeValuesArray, refVelArray)
        pyplot.ylabel('Encoder Velocity [rad/s]')
        pyplot.xlabel('Time [s]')
        
        pyplot.subplot(2,1,2)
        pyplot.plot(timeValuesArray, PosValuesArray)
        pyplot.plot(timeValuesArray, refPosArray)
        axes=pyplot.gca()
        axes.relim()
        axes.autoscale_view()
        pyplot.draw()
        pyplot.xlabel('Time [s]')
        pyplot.ylabel('Encoder Position [rad/s]')
        
        pyplot.savefig('DataPlot.png')
            
    except KeyboardInterrupt:
        pass
    
    #De init code here
    ser = serial.Serial(port=None)
    keyboard.unhook_all()