''' @file       ControlTask.py
    @brief      Implements closed loop control with motor and encoder drivers
    @details    Implements a finite state machine, shown below, to perform close
                loop control on a motor and encoder system
                See code here: https://bitbucket.org/JonathanCederquist/mechatronics_work/src/master/LabFinal/Part3/ControlTask.py
    @image      html ControlTask.png               
    @author     Jonathan Cederquist
    @date       Last Modified 3/18/21
'''

import utime
import shares
import encoderDriver
import motorDriver
import ClosedLoopController
import pyb
from micropython import const
from array import array
from math import pi

class ControlTask:
    ''' @brief Implements a simple finite state machine, shown below, to contorl motor and encoder
        @image html ControlTask.png
    '''
    
    # Static variables
    S0_INIT = const(0)
    S1_UPDATE_VALUES = const(1)
    S2_END = const(2)
    
    
    def __init__(self, Kp=0, Ki=0, fileName='referenceProfile.csv', period=50, DBG_flag=True):
        ''' @brief      Constructs a ControlTask object
            @details    Creates encoderDriver and motorDriver objects along with a ClosedLoopController
                        object to implement closed loop feedback control
            @param      Kp Value of proportional gain
            @param      fileName A string of the file holding desired velocity and position profiles (includes file extension)
            @param      period The period of the FSM in milliseconds
            @param      DBG_flag A boolean value to turn on/off debug mode
        '''
        ## @brief Value of proportional gain if not omitted
        self.Kp = Kp
        
        ## @brief Value of integral gain if not omitted
        self.Ki = Ki
        
        ## @brief File name of csv reference profile if not omitted
        self.fileName = fileName
        
        ## @brief Period of FSM in milliseconds if not omitted
        self.period = period
        
        ## @brief Debug flag to specify if not omitted
        self.DBG_flag = DBG_flag
        
        ## @brief Array for reference times
        self.timesArray = array('f', [0]*int((15000/self.period)+1))
        
        ## @brief Array for reference speed
        self.velocityArray = array('f', self.timesArray)
        
        ## @brief Array for reference position
        self.positionArray = array('f', self.timesArray)
        
        # Repopulate arrays with correct values
        self.getReferenceProfile()
        
        ## @brief Time of next iteration of FSM
        self.nextTime = utime.ticks_add(utime.ticks_ms(), self.period)
        
        ## @brief encoderDriver object 1
        self.enc1 = encoderDriver.encoderDriver(4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, DBG_flag=False)
        
        # Create pins  and timer for motor
        sleepPin = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
        pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
        
        tim = pyb.Timer(3, freq=20000)
        
        ## @brief motorDriver object 1
        self.moe1 = motorDriver.motorDriver(sleepPin, pin_IN1, pin_IN2, tim, 1, 2)
        self.moe1.enable()
        
        ## @brief closedLoopController object
        self.clo = ClosedLoopController.ClosedLoopController(self.Kp, self.Ki)
        
        ## @brief Current state of FSM
        self.state = self.S0_INIT
        
        ## @brief Counter for number of FSM executions
        self.runs = 0
        
        ## @brief Metric for Error of Controller
        self.J = 0
        
        
    def run(self):
        ''' @brief Executes one iteration of the FSM
        '''
        now = utime.ticks_ms()
        
        if utime.ticks_diff(now, self.nextTime) >= 0:
            self.nextTime = utime.ticks_add(now, self.period)
            
            if self.state == self.S0_INIT:
                # Run state 0 code
                
                    
                # Check for user input of Kp and updates value in controller
                if shares.Kp != 0 and shares.Ki != 0:
                    self.Kp = shares.Kp
                    self.Ki = shares.Ki
                    self.clo.set_Kp(self.Kp)
                    self.clo.set_Ki(self.Ki)
                    shares.FORCE_START = True
                    self.runs = 0 #Reset to line up with array indices
                    
                    self.transitionTo(self.S1_UPDATE_VALUES)
                    
            elif self.state == self.S1_UPDATE_VALUES:
                # Run state 1 code
                
                # Force encoder to update
                self.enc1.update()
                
                shares.encoder1_Position = self.enc1.get_Position()
                shares.encoder1_Delta = self.enc1.get_Delta()
                shares.encoder1_Velocity = shares.encoder1_Delta/(self.period/1000)
                try:
                    shares.omega_des = self.velocityArray[self.runs-1]
                    shares.theta_des = self.positionArray[self.runs-1]
                
                    #Update gain from controller
                    L = self.clo.run(shares.omega_des, shares.encoder1_Velocity, shares.theta_des, shares.encoder1_Position)
                except IndexError:
                    print('Something seems to be wrong, array index was out of range')
                    print(self.runs)
                    L=0
                
                #Set motor to new level
                self.moe1.set_duty(L)
                
                #Compute J metric
                self.J += (shares.omega_des-shares.encoder1_Velocity)**2+(shares.theta_des-shares.encoder1_Position)**2
                
                if shares.END:
                    self.transitionTo(self.S2_END)
                    self.count = 0
                    self.J = self.J/self.runs
                    shares.J = self.J
                    
            elif self.state == self.S2_END:
                
                if self.count==0:
                    self.moe1.disable()
                    self.count += 1
        
            self.runs += 1
    
    def transitionTo(self, newState):
        ''' @brief Changes the state of the FSM to newState
            @param newState The new value for the FSM state
        '''
        if self.DBG_flag:
            print('Control: S' + str(self.state) + ' --> S' + str(newState))
        
        self.state = newState
        
    def getReferenceProfile(self):
        ''' @brief Creates an three arrays of desired time, velocity, and position based off given csv reference
        '''
        with open(self.fileName, 'r') as f:
            
            index = 0
            
            for row in f:
                lineStr = row.strip()
                lineArray = lineStr.split(',')
                self.timesArray[index] = float(lineArray[0])
                self.velocityArray[index] = float(lineArray[1])*(2*pi/60)
                self.positionArray[index] = float(lineArray[2])*(pi/180)
                index += 1
            
            if self.DBG_flag:
                print('Processing row' + str(index))
                print(str(row))
                
            
                
