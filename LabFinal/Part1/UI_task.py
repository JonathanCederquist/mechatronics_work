''' @file       UI_task.py
    @brief      Gathers and packages data when prompted by the user
    @details    Implements a finite state machine, shown below, to use serial
                interaction to control time data collection, which is fed through
                a function and returned in a CSV format
    @image      html DataGatherFSM.png
                See code here: 
    @author     Jonathan Cederquist
    @date       Last modified 3/13/2021
'''

from micropython import const
import utime
from math import exp, sin, pi
import pyb
from pyb import UART
import shares

class UI_task:
    ''' @brief      Implements a simple finite state machine to gather data when prompted
        @details    Creates a CSV list of times and associated values given by a decaying
                    exponential sine function when user prompts data collection.
    '''
    
    #Define static variables of states
    S0_INIT = const(0)
    S1_GATHER_DATA = const(1)
    S2_SEND_DATA = const(2)
    S3_END = const(3)
    
    def __init__(self, period=80, DBG_flag=True):
        ''' @brief      Constructs a sineGatherData object
            @details    Initializes two arrays to hold times and function values
            @param      self The object being constructed
            @param      period The period of the FSM in milliseconds
        '''
        ## @brief Period of FSM in milliseconds if not omitted
        self.period = period
        
        ## @brief Debug flag to specify if not omitted
        self.DBG_flag = DBG_flag
        
        ## @brief Time of next iteration
        self.nextTime = utime.ticks_add(utime.ticks_ms(), self.period)
        
        ## @brief Start of data collection time
        self.dataStartTime = utime.ticks_ms()
        
        ## @brief List to hold time values
        self.timeList = []
        
        ## @brief List to hold function values
        self.valuesList = []
        
        ## @brief Index counter for arrays
        self.index = 0
        
        ## @brief Variable for value of serial transmission
        self.value = 0
        
        ## @brief Current state of finite state machine
        self.state = self.S0_INIT
        
        ## @brief UART Input
        self.userUART = UART(2)
        
        ## Disable REPL
        pyb.repl_uart(None)
        
        
        
    def run(self):
        ''' @brief  Executes one iteration of the finite state machine
        '''
        
        now = utime.ticks_ms()
        
        if utime.ticks_diff(now, self.nextTime) >= 0:
            self.nextTime = utime.ticks_add(now, self.period)
            
            if self.state==self.S0_INIT:
                #Run state 0 code
                
                if self.userUART.any() != 0:
                    value = self.userUART.readchar()
                    
                    if value==103:
                        self.transitionTo(self.S1_GATHER_DATA)
                        self.dataStartTime = utime.ticks_ms()

            elif self.state==self.S1_GATHER_DATA:
                # Run state 1 code
                currentTime = utime.ticks_diff(now, self.dataStartTime)/1000

                
                self.timeList.append(currentTime)
                self.valuesList.append(self.computeValue(currentTime))
                
                if self.DBG_flag:
                    print(len(self.timeList))
                
                self.index += 1
                
                if self.userUART.any() != 0:
                    self.value = self.userUART.readchar()
                
                if len(self.timeList) >= int(30000/self.period) or self.value == 115:
                    self.value = 0
                    self.transitionTo(self.S2_SEND_DATA)
                    self.index = 0
                    self.period = 10
                    
            elif self.state==self.S2_SEND_DATA:
                # Run state 2 code
                if self.index < len(self.timeList):
                    self.userUART.write('{:}, {:}\r\n'.format(self.timeList[self.index], self.valuesList[self.index]))
                
                    self.index += 1
                
                elif self.index >= len(self.timeList):
                    self.transitionTo(self.S3_END)
                    self.doneCount = 0
            
            elif self.state==self.S3_END:
                # Run state 3 code
                if self.doneCount == 0:
                    self.userUART.write('')
                    self.doneCount = 1
                    print('All done sending data!')
                pass
            
            else:
                # FSM should never reach here
                pass
            
            
    def computeValue(self, time):
        ''' @brief  Computes value of function given a time value
            @param  time Time in seconds to use in formula
            @return The value of a decaying exponential function
        '''
        return exp(-time/10)*sin(2*pi/3*time)
    
    def transitionTo(self, newState):
        ''' @brief  Changes the state of the FSM to newState
            @param  newState The state to transition to
        '''
        
        if self.DBG_flag:
            print('S' + str(self.state) + ' --> S' + str(newState))
        
        self.state = newState
        
    def retrievePosition1(self):
        ''' @brief  Retrieves the position value for encoder 1 from shares
            @return The current position of encoder 1 (in radians)
        '''
        return shares.encoder1_Position
    
    def retrievePosition2(self):
        ''' @brief  Retrieves the position value for encoder 2 from shares
            @return The current position of encoder 2 (in radians)
        '''
        return shares.encoder2_Position
    
    def retrieveDelta1(self):
        ''' @brief  Retrieves the delta value for encoder 1 from shares
            @return The current delta of encoder 1 (in radians)
        '''
        return shares.encoder1_Delta
    
    def retrieveDelta2(self):
        ''' @brief  Retrieves the delta value for encoder 2 from shares
            @return The current delta of encoder 2 (in radians)
        '''
        return shares.encoder2_Delta
    
    def zeroEnc1(self):
        ''' @brief  Resets the position of encoder 1 to 0
        '''
        shares.encoder1_Position = 0
        
    def zeroEnc2(self):
        ''' @brief  Resets the position of encoder 2 to 0
        '''
        shares.encoder2_Position = 0
        
