''' @file       motorDriver.py
    @brief      Controls the motors on the board
    @details    Uses alternate functions of timer channels to implement DC motor
                with pulse width modulation
                See code here: https://bitbucket.org/JonathanCederquist/mechatronics_work/src/master/LabFinal/Part3/motorDriver.py
    @author     Jonathan Cederquist
    @date       Last Modified 3/04/21
'''
import pyb

class motorDriver:
    ''' @brief Class to control the motor
    '''

    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer, channel1, channel2, DBG_flag=True):
        ''' @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
            @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
            @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
            @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
            @param timer A pyb.Timer object to use for PWM generation on
                    IN1_pin and IN2_pin.
            @param channel1 An int for the timer channel associated with IN1_pin
            @param channel2 An int for the timer channel associated with IN2_pin
            @param DBG_flag A boolean value to turn on/off debug mode
        '''
        ## @brief Sleep Pin
        self.EN = nSLEEP_pin
        
        ## @brief Timer used to control PWM
        self.timer = timer
        
        ## @ brief Timer channel 1 for PWM
        self.timch1 = self.timer.channel(channel1, mode=pyb.Timer.PWM, pin=IN1_pin)
        
        ## @ brief Timer channel 2 for PWM
        self.timch2 = self.timer.channel(channel2, mode=pyb.Timer.PWM, pin=IN2_pin)
        
        ## @brief Debug flag to specify if not omitted
        self.DBG_flag = DBG_flag
        
        
        
    def enable (self):
        ''' @brief  Enables the motor by setting the sleep pin to high
        '''
        
        if self.DBG_flag:
            print ('Enabling Motor')
        
        self.EN.value(1)
    
    def disable (self):
        ''' @brief  Disables the motor by setting the sleep pin to low
        '''
        
        if self.DBG_flag:    
            print ('Disabling Motor')
        
        self.EN.value(0)
    
    def set_duty (self, duty):
        ''' @brief      This method sets the duty cycle to be sent to the motor.
            @details    Positive values cause effort in one direction, negative values in the opposite direction.
            @param      duty A signed integer holding the duty cycle of the PWM signal sent to the motor 
        '''
        if self.DBG_flag:
            print('Setting the motor to a duty cycle of ' + str(duty))
            
        if duty > 0:
            self.timch2.pulse_width_percent(0)
            self.timch1.pulse_width_percent(duty)
        elif duty < 0:
            self.timch1.pulse_width_percent(0)
            self.timch2.pulse_width_percent(-duty)
        else:
            self.timch1.pulse_width_percent(0)
            self.timch2.pulse_width_percent(0)

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)
    
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq=20000);
    
    # Create a motor object passing in the pins and timer
    moe = motorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim, 1, 2)
    moe2 = motorDriver(pin_nSLEEP, pin_IN3, pin_IN4, tim, 3, 4)
    
    
    # Enable the motor driver
    moe.enable()
    
    # Set the duty cycle to 10 percent
    moe.set_duty(50)
    pyb.delay(500)
    moe2.set_duty(80)
    pyb.delay(500)
    moe.set_duty(0)
    moe2.set_duty(0)
    pyb.delay(500)
    moe.set_duty(-80)
    pyb.delay(500)
    moe2.set_duty(-50)
    pyb.delay(500)
    moe.disable()
