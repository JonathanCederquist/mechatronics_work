''' @file       shares.py
    @brief      Container file holding all relevant variables for motor control project
    @details    See code here: https://bitbucket.org/JonathanCederquist/mechatronics_work/src/master/LabFinal/Part3/shares.py
    @author     Jonathan Cederquist
    @date       Last Modified 3/18/21
'''
## Relevant Variables

## @brief Encoder 1 position in radians
encoder1_Position = 0

## @brief Encoder 2 position in radians
encoder2_Position = 0

## @brief Encoder 1 Delta in radians
encoder1_Delta = 0

## @brief Encoder 2 Delta in radians
encoder2_Delta = 0

## @brief Encoder 1 Speed in radians/s
encoder1_Velocity = 0

## @brief Encoder 2 Speed in radians/s
encoder2_Velocity = 0

## @brief Desired speed of encoder 1 [rad/s]
omega_des = 0

## @brief Desired position of encoder 1 [rad]
theta_des = 0

# Performance Metric
J = 0

## @brief Proportional gain
Kp = 0

## @brief Integral gain
Ki = 0

## @brief Flag for force starting encoder
FORCE_START = False

## @brief Flag for ending program
END = False