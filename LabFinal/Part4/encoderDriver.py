''' @file       encoderDriver.py
    @brief      Used to read from encoders attached to motors on the board
    @details    Utilizes alternate functions from timer channels on pyb board to 
                read from attached encoders
                See code here: https://bitbucket.org/JonathanCederquist/mechatronics_work/src/master/LabFinal/Part3/encoderDriver.py
    @author     Jonathan Cederquist
    @date       Last Modified 3/13/21
'''

import pyb
from math import pi

class encoderDriver:
    ''' @brief  Drives the encoder
    '''
    
    def __init__(self, timerNum, pin1, pin2, DBG_flag=True):
        ''' @brief      Constructs an encoderDriver object
            @details    Uses the alternate functions associated with the given pins
                        to create two timer channels in an encoder mode that
                        can be used to read the position of the encoder
            @param      timerNum An int used to create the corresponding pyb.Timer object
                        to use with timer channels to read from the encoder
            @param      pin1 A pyb.Pin object to use for channel 1 of the encoder. Must 
                        match the alternate functions available in the Nucleo documentation
                        for the Timer object described above
            @param      pin2 A pyb.Pin object to use for channel 2 of the encoder. Must 
                        match the alternate functions available in the Nucleo documentation
                        for the Timer object described above
            @param      DBG_flag A boolean flag to turn on/off debug mode
        '''
        ## @brief Max period
        self.period = 65535
        
        ## @ brief Timer object to use with encoder
        self.timer = pyb.Timer(timerNum, prescaler=0, period=self.period)
        
        ## @ brief Set debug flag if not omitted
        self.DBG_flag = DBG_flag
        
        ## @ brief Timer channel 1 corresponding to pin 1
        self.timch1 = self.timer.channel(1, mode=pyb.Timer.ENC_AB, pin = pin1)
        
        ## @ brief Timer channel 2 corresponding to pin 2
        self.timch2 = self.timer.channel(2, mode=pyb.Timer.ENC_AB, pin = pin2)
        
        ## @ brief Current position of the encoder
        self.position = 0
        
        ## @ brief Previous position of the encoder
        self.lastPosition = 0
        
        ## @ brief Difference in position between update calls
        self.delta = 0
        
        ## @ brief Current encoder value (can over/underflow)
        self.value = 0
        
        ## @ brief Previous encoder value
        self.lastValue = 0
        
    def update(self):
        ''' @brief  Updates the values of the encoder based on current and previous position
        '''
        
        # Set last encoder count to current count, then update current count
        self.lastValue = self.value
        self.value = self.timer.counter()
        
        if self.DBG_flag:
            print('Updating encoder position')
            print('current timer position: '+ str(self.value))
        
        # Raw delta value to be checked
        checkDelta = self.value - self.lastValue
        
        if checkDelta > -self.period/2 and checkDelta < self.period/2:
            delta = checkDelta
        
        elif checkDelta > int(self.period/2):
            delta = checkDelta - self.period
            
        elif checkDelta < int(-self.period/2):
            delta = checkDelta + self.period
        
        # Convert to actual position in rad
        self.delta = delta*(pi/2000) 
        
        # Set last position to current position, then update current position
        self.lastPosition = self.position
        self.position = self.lastPosition + self.delta
        
        
    
    def get_Position(self):
        ''' @brief  Provides current position of encoder in radians
            @return The encoder's current position (cumulative)
        '''
        return self.position
    
    def set_Position(self, posValue):
        ''' @brief  Allows user to reset or set position of encoder
            @param  posValue The updated value the encoder position will be set to
        '''
        self.position = posValue
        
    def get_Delta(self):
        ''' @brief  Provides difference in position between two most recent update calls
            @return The delta (difference) between current and last position
        '''
        return self.delta
    
if __name__ == '__main__':
    
    timNum1 = 4
    timNum2 = 8
    pin1 = pyb.Pin(pyb.Pin.cpu.B6)
    pin2 = pyb.Pin(pyb.Pin.cpu.B7)
    pin3 = pyb.Pin(pyb.Pin.cpu.C6)
    pin4 = pyb.Pin(pyb.Pin.cpu.C7)
    
    enc1 = encoderDriver(timNum1, pin1, pin2)
    enc2 = encoderDriver(timNum2, pin3, pin4)
    
    while True:
        try:
            enc1.update()
            enc2.update()
            
            
            print(enc1.get_Position())
            print(enc1.get_Delta())
            
            print(enc2.get_Position())
            print(enc2.get_Delta())
            
            pyb.delay(500)
        except KeyboardInterrupt:
            break
    
    enc1.set_Position(0)
    #enc2.set_Position(0)
    
    #print(enc1.getPosition())
    
        
    
        