''' @file       UIFrontEnd.py
    @brief      Queries user for input to control data collection
    @details    Communicates with UI_task running on Nucleo to accept user
                input to control data collection of a simple decaying sine
                function
                <br>See code here: https://bitbucket.org/JonathanCederquist/mechatronics_work/src/master/LabFinal/Part2/UIFrontEnd.py
                <br>See example CSV here: https://bitbucket.org/JonathanCederquist/mechatronics_work/src/master/LabFinal/Part2/data.csv
                <br>An example plot that is generated:
    @image      html DataPlot.png
    @author     Jonathan Cederquist
    @date       Last modified 3/15/2021
'''

import serial
from matplotlib import pyplot
from array import array
import keyboard
import csv

def onKeyPress(key):
        ''' @brief Callback function to run when key is pressed.
            @param key The key that is pressed
        '''
        global key_pressed 
        key_pressed = key.name 
        

if __name__ == '__main__':
    
    #Set up Nucleo link
    ser = serial.Serial(port='COM7', baudrate=115273, timeout=2)
    timeValues = []
    funcValues = []
    refVel = []
    
    key_pressed = None
    
    ## Turn on callback for particular keys only
    keyboard.on_release_key('s', callback=onKeyPress)
    keyboard.on_release_key('g', callback=onKeyPress)
    keyboard.on_release_key('z', callback=onKeyPress)
    keyboard.on_release_key('p', callback=onKeyPress)
    keyboard.on_release_key('d', callback=onKeyPress)
    
    #Create CSV Writer
    
        
        
    try:
        print('Welcome to the simple data collection demonstration!')
        print('Please input a ''g'' to begin gathering data. The Nucleo will then start gathering'
              ' values of encoder positions')
        print('You may also press ''p'', ''d'', or ''z'' at any time to print the encoder position,'
              ' encoder delta, or zero the encoder, respectively.')
        print('To stop the data collection, simply press ''s'' or wait for 30 seconds'
              ' Either of those events will initiate data transfer.')
        print('Once the data has been sent,  the script will generate a plot of the encoder position'
              ' and a CSV file of the data.')
        
        # Is it ok to have blocking code for gain?
        Kp = 'k'+input('Please enter a value for the proportional gaing as a decimal number:')
        ser.write('{:}\r\n'.format(Kp).encode())
        key_pressed = 'g' #Manually rig while loop?
        
        
        while True:
            
            if key_pressed == 'g':
                ser.write(key_pressed.encode())
                key_pressed = None
                print('Thank you for starting data collection. Please wait while data is collected. . .')
                break
            elif key_pressed == 'z':
                print('You pressed z!')
                ser.write(key_pressed.encode())
                key_pressed = None
                print('Encoder position has been zeroed')
            elif key_pressed == 'p':
                print('You pressed p!')
                ser.write(key_pressed.encode())
                key_pressed = None
                try:
                    value = ser.readline().decode()
                    print('Encoder position is: {:} rad'.format(value))
                except:
                    print('something went wrong')
            elif key_pressed == 'd':
                print('You pressed d!')
                ser.write(key_pressed.encode())
                key_pressed = None
                try:
                    value = ser.readline().decode()
                    print('Encoder delta is: {:} rad'.format(value))
                except:
                    print('something went wrong')
            elif key_pressed != 'g':
                key_pressed = None

        
        t_Max = 0
        
        while ser.in_waiting == 0:
            if key_pressed == 's':
                    ser.write(key_pressed.encode())
                    key_pressed = None
                    break
            elif key_pressed == 'z':
                print('You pressed z!')
                ser.write(key_pressed.encode())
                key_pressed = None
                print('Encoder position has been zeroed')
            elif key_pressed == 'p':
                print('You pressed p!')
                ser.write(key_pressed.encode())
                key_pressed = None
                try:
                    value = ser.readline().decode()
                    print('Encoder position is: {:} rad'.format(value))
                except:
                    print('something went wrong')
            elif key_pressed == 'd':
                print('You pressed d!')
                ser.write(key_pressed.encode())
                key_pressed = None
                try:
                    value = ser.readline().decode()
                    print('Encoder delta is: {:} rad'.format(value))
                except:
                    print('something went wrong')
            t_Max += 1
            if t_Max > 10000000:
                print('Sorry, the serial line took too long to respond')
                break
        
        count = 0
        
        
        myStr = ser.readline().decode()
        while myStr != '':
            print('Receiving Data')
            myStr.strip()
            mySplit = myStr.split(', ')

            
            timeValues.append(float(mySplit[0]))
            funcValues.append(float(mySplit[1]))
            refVel.append(float(mySplit[2]))
            
            myStr = ser.readline().decode()
            
            
            count += 1
        
        print(len(timeValues))
        timeValuesArray = array('f', [0]*len(timeValues))
        funcValuesArray = array('f', [0]*len(funcValues))
        refVelArray = array('f', [0]*len(refVel))
        
        with open('data.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            for i in range(len(timeValuesArray)):
                timeValuesArray[i] = timeValues[i]
                funcValuesArray[i] = funcValues[i]
                refVelArray[i] = refVel[i]
                writer.writerow([str(timeValuesArray[i]), str(funcValuesArray[i]), str(refVelArray[i])])
       
        
        print('Finished collecting data, now generating a plot') 
        pyplot.figure(1)
        pyplot.plot(timeValuesArray, funcValuesArray)
        pyplot.plot(timeValuesArray, refVelArray)
        axes=pyplot.gca()
        axes.relim()
        axes.autoscale_view()
        pyplot.draw()
        pyplot.xlabel('Time [s]')
        pyplot.ylabel('Encoder/Reference Velocity [rad/s]')
        pyplot.savefig('DataPlot.png')
            
    except KeyboardInterrupt:
        pass
    
    #De init code here
    ser = serial.Serial(port=None)
    keyboard.unhook_all()