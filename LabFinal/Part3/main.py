''' @file       main.py
    @brief      Creates and indefinitely runs tasks
    @author     Jonathan Cederquist
    @date       Last Modified 3/18/21
'''    

import UI_task
import ControlTask
import shares

if __name__ == '__main__':
    
    UITask = UI_task.UI_task()
    ControlTask = ControlTask.ControlTask(fileName='Week3Ref.csv')
    
    
    while True:
        
        try:
            UITask.run()
            ControlTask.run()
            
        except KeyboardInterrupt:
            break
    
    print('Goodbye!')
        
        