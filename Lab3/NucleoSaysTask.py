''' @file       NucleoSaysTask.py
    @brief      Utilizes NucleoSays class to play a game of Simon Says
    @details    Imports the NucleoSays class and runs the task until a
                keyboard interrupt is triggered. 
    @author     Jonathan Cederquist
    @author     Kristin Kraybill-Voth
    @date       Last Modified 2/17/21
'''

# Import NucleoSays class
import NucleoSays


if __name__ == "__main__":
    #Program initialiation goes here
    
    nucleoTask = NucleoSays.NucleoSays(rig_Pat=False) 
    
    # Runs the task until an interrupt is pressed
    while True:
        try:
            nucleoTask.run()
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            break
    
        
    # Program de-initialization
    nucleoTask.deInit()
    print('Thanks for playing the game!')