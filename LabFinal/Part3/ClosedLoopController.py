''' @file       ClosedLoopController.py
    @brief      Implements a simple proportional controller
    @details    See code here: 
    @author     Jonathan Cederquist
    @date       Last Modified 3/14/21
'''
class ClosedLoopController:
    ''' @brief Implements a simple proportional control
    '''
    
    def __init__(self, Kp, PWM_limit=100):
        ''' @brief Constructs a closed loop controller object with gain Kp
            @param Kp Initial gain of the proportional controller
            @param PWM_limit Max level of that pulse width modulation can be set
        '''
        
        ## @brief Proportional gain
        self.Kp = Kp
        
        ## @brief Limit for max value of PWM
        self.PWM_limit = PWM_limit
        
    def run(self, desiredValue, currentValue):
        ''' @brief  Computes the value of the output signal based on input values
            @param  desiredValue The desired value to be obtained
            @param  currentValue The current value input to the controller
            @return The output actuation value based on the gain and inputs
        '''
        error = desiredValue - currentValue
        
        toReturn = self.Kp*error
        
        if toReturn > -self.PWM_limit and toReturn < self.PWM_limit:
            return toReturn
        elif toReturn < -self.PWM_limit:
            return -self.PWM_limit
        else:
            return self.PWM_limit
    
    def get_Kp(self):
        ''' @brief  Returns the current value of proportional gain Kp
            @return The current value of Kp
        '''
        return self.Kp
    
    def set_Kp(self, newKp):
        ''' @brief Sets the proportional gain Kp to newKp
            @param newKp The new value for the gain Kp
        '''
        self.Kp = newKp